import trankit
from collections import namedtuple
from dataclasses import dataclass

from datastructures import Token, Article

"""Script permettant d'analyser linguistiquement les tokens des articles. Nous avons le choix avec 3 parser : trankit, stanza et spacy. Nous allons preferer Stanza car + performant."""

#permet la création du parser
def create_parser_t():
    return trankit.Pipeline('french', gpu=False)
#fonction qui permet l'analyse syntaxique avec Trankit et return les résultats à l'objet article
def trankito(parser, article: Article) -> Article:
     # lance le parser Trankit sur le titre et la description de l'article
    result = parser((article.titre or "") + "\n" + (article.description or ""))
    #liste qui stock le resultat
    output = []
    #parcourt les phrases dans les résultats.
    for sentence in result['sentences']:
        #puis les token dans chaque phrase
        for token in sentence['tokens']:
            # si le token n'a pas d'attribut 'expanded', on l'initialise avec une liste contenant le token lui-même
            if 'expanded' not in token.keys():
                token['expanded'] = [token]
            for w in token['expanded']:
            #nous pouvons voir qu'il va extraire la forme , le lemme et la part of speech de chaque token, puis on ajoute a la liste qui stock les resulats
                output.append(Token(w['text'], w['lemma'], w['upos']))

    article.analyse = output
    #nous retournons une liste qui contiendra l'analyse
    return article
import stanza
#idem ici permet la création du parser. la fonction fonctionne de la même manière qu'au dessus 
def create_parser_s():
    return stanza.Pipeline(lang='fr', processors='tokenize,mwt,pos,lemma')

def etiquetage_stanza(parser, article: Article) -> Article:
    result = parser((article.titre or "") + "\n" + (article.description or ""))
    output = []
    print(result)
    # parcourt les phrases dans les résultats
    for sentence in result.sentences:
        sentence_tokens = []
        #parcourt les mots dans chacune des phrases
        for word in sentence.words:
            #nous pouvons voir qu'il va extraire la forme , le lemme et la part of speech de chaque token.
            output.append(Token(word.text, word.lemma, word.upos))

    article.analyse = output
    #nous retournons une liste qui contiendra l'analyse
    return article



import spacy

def spacy_tagging(texte, L):
    # Chargement du modéle d'annotation du français le plus précis de Spacy
    nlp = spacy.load("fr_dep_news_trf")
    # Tokenization et traitement de la str
    doc = nlp(texte)

    for token in doc:
        # On ignore les caractères d'espacement ou de retour à la ligne
        if token.text == " " or token.text == "\n":
            continue
        else:
            a,b,c,d = token.text, token.lemma_, token.pos_, token.dep_
            L.append({'form' : a,'lemma' : b, 'pos' : c, 'syntactic dependency' : d})
    return L
