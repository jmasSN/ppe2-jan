import pickle
from dataclasses import asdict
from datastructures import Corpus, Article
def write_pickle(corpus:Corpus, destination:str):
    with open(destination, 'ab') as f:
        pickle.dump(corpus, f)
    with open(destination, 'rb') as f2:
        bou=pickle.load(f2, fix_imports=True, encoding='utf-8', errors='strict', buffers=None)
    print(bou)
