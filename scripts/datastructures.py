from typing import List, Dict, Optional
from dataclasses import dataclass
from pathlib import Path

#contiens les classes qui vont nous interesser dans le cadre de ce projet. Le traitement sera beaucoup plus simple avec des classes, la manipulation et le code est plus lisible.

@dataclass

#class qui sert pour l'analyse linguistique
class Token:
    forme: str
    lemme: str
    pos: str   


@dataclass
#class qui sert lors de l ecriture du corpus à stocker l'intégralité des données de nos articles
class Article:
    titre: str
    description: str
    date : str
    categorie : str
    analyse: Optional[List[Token]] = None

@dataclass
#class qui engloble notre corpus en mentionnant les catégories extraites , la fourchette de date, le chemin et qui contient la liste des articles.
class Corpus:
    categories: List[str]
    begin: str
    end: str
    chemin: Path
    articles: List[Article]


