r"""
LDA Model VERSION FINALE
=========

Introduces Gensim's LDA model and demonstrates its use on the NIPS corpus.

"""
""" python3 topicmodel.py -entree xml -file corpus.xml -filtre_pos ADJ -tokenf_l forme -o sortie.html
"""

import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
import json

from xml.etree import ElementTree as ET

import argparse

from typing import List,Optional
import sys
import argparse


from gensim.models import Phrases
from gensim.corpora import Dictionary
from gensim.models import LdaModel
import pyLDAvis
import pyLDAvis.gensim_models as gensimvis
import xml.etree.ElementTree as ET
import pickle
import json

#permet de charger un fichier JSON contenant des articles et extrait les tokens correspondants en fonction des filtres de POS et de token.

def load_json(json_file:str,filtre_pos,tokenf_l) -> List[List[str]]: 
    with open(json_file, "r") as json_file:
            docs=[] # liste qui stocke chaque article
            jsonObj = json.load(json_file)
            articles = jsonObj["articles"] #extrait les articles
            for article in articles:
                doc = [] # liste qui stocke les tokens d'un article
                for analyse in article['analyse']:
                    if filtre_pos != "":
                        if analyse['pos'] in filtre_pos:
                            doc.append(f"{analyse[tokenf_l]}")
                    else:
                        doc.append(f"{analyse[tokenf_l]}")
                if len(doc) > 0:
                    docs.append(doc)
    return docs


#permet de charger un fichier xml contenant des articles et extrait les tokens correspondants en fonction des filtres de POS et de token.


def load_xml(file,filtre_pos,tokenf_l):
    tree = ET.parse(file)
    root = tree.getroot() # récupére  la racine du fichier ( le tree ) XML
    docs = [] #liste qui stock chaque article
    for elem in root.iter('article'):
        doc = [] #liste qui stock les tokens par article
        for element in elem.find('analyse').iter('token'):
            if element.get('pos') in filtre_pos and len(element.get('forme'))>2 and element.get('forme') not in tokenf_l:
                doc.append(element.get('forme').lower())
        docs.append(doc)
    return docs

#permet de charger un fichier pickle contenant des articles et extrait les tokens correspondants en fonction des filtres de POS et de token.

def load_pickle(pickle_file: str, filtre_pos: str, tokenf_l: str) -> List[List[str]]:
    with open(pickle_file, "rb") as f:
        data = pickle.load(f)
        docs = []
        for article in data.articles:
            doc = []
            for token in article.analyse:         
                if len(str(token)) > 1 and token.forme not in ".,":
                    if filtre_pos == '' or token.pos == filtre_pos:
                        if tokenf_l == "lemma":
                            token_label = f"{token.lemme}"
                        else:
                            token_label = f"{token.forme}"
                        doc.append(token_label)
            if len(doc) > 0:
                docs.append(doc)

    return docs


# Add bigrams and trigrams to docs (only ones that appear 20 times or more).

def add_bigrams(docs: List[List[str]], min_count=20):
    bigram = Phrases(docs, min_count=20)
    for idx in range(len(docs)):
        for token in bigram[docs[idx]]:
            if '_' in token:
                # Token is a bigram, add to document.
                docs[idx].append(token)
    return docs

def build_lda_model(
        docs: List[List[str]],
        num_topics = 10,
        chunksize = 2000,
        passes = 20,
        #iterations = 400,
        iterations = 150,
        eval_every = None,
        #no_below=1,
        no_below=1,
        no_above=0.5
        ):


    dictionary = Dictionary(docs)  # création d un dictionnaire à partir des documents
    dictionary.filter_extremes(no_below=no_below, no_above=no_above) #permet la filtration en fonction du no bellow et no above
    corpus = [dictionary.doc2bow(doc) for doc in docs]
    print('Number of unique tokens: %d' % len(dictionary),sys.stderr)
    print('Number of documents: %d' % len(corpus))

    temp = dictionary[0]  # "Cela sert uniquement à "charger" le dictionnaire."
    id2word = dictionary.id2token
# construction du modèle LDA
    model = LdaModel(
        corpus=corpus,
        id2word=id2word,
        chunksize=chunksize,
        alpha='auto',
        eta='auto',
        iterations=iterations,
        num_topics=num_topics,
        passes=passes,
        eval_every=eval_every)
    return corpus, dictionary, model

def print_coherence(model, corpus):
    top_topics = model.top_topics(corpus)

# Average topic coherence is the sum of topic coherences of all topics, divided by the number of topics.
    avg_topic_coherence = sum([t[1] for t in top_topics]) / model.num_topics
    print('Average topic coherence: %.4f.' % avg_topic_coherence)

    from pprint import pprint
    pprint(top_topics)



def save_html_viz(model, corpus, dictionary, output_path):
    vis_data = gensimvis.prepare(model, corpus, dictionary)
    with open(output_path, "w") as f:
        pyLDAvis.save_html(vis_data, f)




def main(entre,corpus_file:str,filtre_pos,tokenf_l,num_topics, output_path: Optional[str]=None, show_coherence: bool=False):
    if entre=="json":
        docs = load_json(corpus_file,filtre_pos,tokenf_l)
    if entre=="xml":
        docs = load_xml(corpus_file,filtre_pos,tokenf_l)
    if entre=="pickle":
        docs = load_pickle(corpus_file,filtre_pos,tokenf_l)

    c, d, m = build_lda_model(docs, num_topics=num_topics)
    if output_path is not None:
        save_html_viz(m, c, d, output_path)
    if show_coherence:
        print_coherence(m, c)




if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-entree", help="fromat du fichier entré (json, pickle, ou xml)")
    parser.add_argument("-file", help="fichier contenant le corpus à analyser")
    parser.add_argument("-filtre_pos", help="choisir un pos")
    parser.add_argument("-tokenf_l", help="choisir entre lemma et forme")
    parser.add_argument("-n", default=10, type=int, help="nombre de topics (10)")
    parser.add_argument("-o", default=None, help="génère la visualisation ldaviz et la sauvegarde dans le fichier html indiqué")
    parser.add_argument("-c", action="store_true", default=False, help="affiche les topics et leur cohérence")


    args = parser.parse_args()
    main(args.entree,args.file,args.filtre_pos,args.tokenf_l, args.n, args.o, args.c, )
