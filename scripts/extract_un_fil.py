#!/usr/bin/python3

import sys
import re
from pathlib import Path
import feedparser
from xml.etree import ElementTree as et
from datastructures import Article
"""script permettant d'extraire les titres, descriptions, et dates de nos articles présent dans le corpus 2022"""

#regex qui permet d extraite la totalité des données qui nous interesse
regex_item = re.compile("<item><title>(.*?)<\/title>.*?<description>(.*?)<\/description>.*?<pubDate>(.*?)</pubDate>")

#ici nous nettoyons le texte de caractere indésirable tel que cdata..
def nettoyage(texte):
    texte_net = re.sub("<!\[CDATA\[(.*?)\]\]>", "\\1", texte) 
    return texte_net

def extract_re(fichier_rss,categorie):
    
    with open(fichier_rss, "r") as input_rss:
        lignes = input_rss.readlines()
        texte = "".join(lignes)
        for m in re.finditer(regex_item, texte):
            #boucle permettant de filtrer ce que nous voulons soit : titre , description et date, nous utilisons la fonction group() qui permet de decouper notre regex en groupe
            titre_net = nettoyage(m.group(1))

            description_net = nettoyage(m.group(2))
            date_net= nettoyage(m.group(3))
            #on retourne à notre classe les infos qu'on a extraite ainsi qu'une liste vide qui va être remplie de l'analyse avec notre script analyse_ling.py
                
            yield Article(titre_net, description_net, date_net,categorie,[])
                    

def extract_feedparser(fichier_rss,categorie):
    feed = feedparser.parse(fichier_rss)
    for entry in feed['entries']:
        # ici title , summmary et published permettent l extraction de nos 3 données qui nous interessent
        yield Article(entry['title'], entry['summary'],entry["published"], categorie,[])

def extract_et(fichier_rss,categorie):
    text = Path(fichier_rss).read_text()
    if len(text) > 0:
        xml = et.parse(fichier_rss)
        root = xml.getroot()
        for item in root.findall(".//item"):
            #Grace à la fonction item.find il est facile de trouver les données demandées
            title = item.find("title").text
            desc = item.find("description").text
            date = item.find("pubDate").text
            
            yield Article(title, desc, date,categorie, [])

def main(fichier_rss, extract_fun):
    for i, (titre, description) in enumerate(extract_fun(fichier_rss)):
        print(i,titre)
        print(description)
        print("---")
#permet de tester nos fonctions ainsi que de comparer leurs temps.
if __name__ == "__main__":
    fichier_rss = sys.argv[1]
    print("démo RE")
    main(fichier_rss, extract_re)
    print("démo feedparser")
    main(fichier_rss, extract_feedparser)
    print("démo etree")
    main(fichier_rss, extract_et)


