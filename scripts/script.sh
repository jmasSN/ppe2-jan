#!/bin/bash
extract_many() {
    read -p "Entrez le chemin du répertoire contenant le corpus : " chemin

    read -p "Entrez la date de début (au format YYYY-MM-DD) : " start_date


    read -p "Entrez la date de fin (au format YYYY-MM-DD) : " end_date


    

    read -p "Entrez le nom du parser (spacy, trankit, stanza) : " parser

    read -p "Entrez le format de sortie (xml, json, pickle) : " output_format

    read -p "Entrez les catégories souhaitées (séparées par un espace) : " categories

    read -p "Entrez le nom de la sortie avec l'extension : " fichiersortie

    python3 extract_many.py -s "$start_date" -e "$end_date" "$chemin" "$categories" -sortie "$output_format" -parser "$parser" -o "$fichiersortie"

    out_file="$fichiersortie"

    echo "Le fichier de sortie s'appelle : $out_file"
}

read -r -p "Voulez-vous récupérer les données des livres entre le 1 er septembre et 1 er novembre ? [y/n] : " choix

if [ "$choix" = "y" ]; then
    # Exécute extract_many pour initialiser la variable input_file
    python3 extract_many.py -s 2022-09-01 -e 2022-11-01 2022 livres -sortie xml -parser stanza -o corpus_livres.xml
else
    # Demande le chemin du fichier à traiter à l'utilisateur
    echo "Entre ton export personnalisé :"
    extract_many
fi

read -r -p "Voulez-vous récupérer les données du sport entre le 20 novembre et 18 décembre ? [y/n] : " choix

if [ "$choix" = "y" ]; then
    # Exécute extract_many pour initialiser la variable input_file
    python3 extract_many.py -s 2022-11-20 -e 2022-12-18 2022 livres -sortie xml -parser stanza -o corpus_sport.xml
else
    # Demande le chemin du fichier à traiter à l'utilisateur
    echo "Entre ton export personnalisé :"
    extract_many
fi

read -r -p "Voulez-vous récupérer les données de science et technologie entre le 1 er decembre et le 15 decembre ? [y/n] : " choix

if [ "$choix" = "y" ]; then
    # Exécute extract_many pour initialiser la variable input_file
    python3 extract_many.py -s 2022-12-01 -e 2022-12-15 2022 science technologie -sortie xml -parser stanza -o corpus_science_technologie.xml
else
    # Demande le chemin du fichier à traiter à l'utilisateur
    echo "Entre ton export personnalisé :"
    extract_many


read -r -p "Voulez-vous récupérer les données du cinéma entre le 1er fevrier et 31 mars ? [y/n] : " choix

if [ "$choix" = "y" ]; then
    # Exécute extract_many pour initialiser la variable input_file
    python3 extract_many.py -s 2022-02-01 -e 2022-03-31 2022 cinema -sortie xml -parser stanza -o corpus_cinema.xml
else
    # Demande le chemin du fichier à traiter à l'utilisateur
    echo "Entre ton export personnalisé :"
    extract_many
fi

while true; do
# Demande le chemin du fichier à traiter à l'utilisateur
read -r -p "Voulez-vous récupérer les données de science et technologie entre le 1er décembre et le 15 décembre ? [y/n/q] : " choix
if [ "$choix" = "y" ]; then
extract_many
   elif [ "$choix" = "q" ]; then
            echo "Programme terminé."
            break 
fi
