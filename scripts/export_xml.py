from typing import List
import argparse
from xml.etree import ElementTree as ET
from datastructures import Corpus, Article, Token

# fonction prend une liste de tokens en entrée et génère un élément XML représentant leur analyse.
def analyse_to_xml(tokens: List[Token]) -> ET.Element:
    root = ET.Element("analyse")
    for tok in tokens:
        tok_element = ET.SubElement(root, "token")
        tok_element.attrib['forme'] = tok.forme
        tok_element.attrib['pos'] = tok.pos
        tok_element.attrib['lemme'] = tok.lemme
    return root

# fonction prend un objet de la classe Article en entrée et génère un élément XML représentant l'article.
def article_to_xml(article: Article) -> ET.Element:
    # Crée la un nouvel element XML appelé "article".
    art = ET.Element("article")
    # Définit l'attribut "categorie" de l'élément article avec la catégorie de l'article.
    art.attrib['categorie']=article.categorie
    # crée des éléments XML à l'intérieur de l'élément article càd titre,descri,date...
    title = ET.SubElement(art, "title")
    description = ET.SubElement(art, "description")
    date = ET.SubElement(art, "date")
    # permet de définir le texte des éléments title, description et date avec les informations de l'article correspondantes.
    title.text = article.titre
    description.text = article.description
    date.text= article.date
    # permet d'ajouter l'analyse de l'article sous forme d'élément XML à l'élément article
    art.append(analyse_to_xml(article.analyse))
    return art

#fonction permettant l'écriture du corpus. L'element corpus est considéré comme la 'root' puis nous lui ajoutons deux attributs 'begin' et 'end' après cela si nous avons plus qu'une catégorie l'attribut catégories sera la concaténation des catégories de l'analyse. content contient le résultat de article_to_xml()
def write_xml(corpus: Corpus, destination: str):
    root = ET.Element("corpus")
    root.attrib['begin'] = corpus.begin
    root.attrib['end'] = corpus.end
    cat= None

    if len(corpus.categories) > 1:
        cat = ", ".join(corpus.categories)
    else:
        cat = corpus.categories[0]
    root.attrib["categories"] = cat
    content = ET.SubElement(root, "content")

    for article in corpus.articles:
        art_xml = article_to_xml(article)
        content.append(art_xml)

    tree = ET.ElementTree(root)
    ET.indent(tree)
    tree.write(destination)
