# **************************************************************************** #
#                                                                              
#    etree.py                                           
#                                                     
#    By: @juliette             
#
#    Created: 2023/03/20 11:20:09                         
#    Updated: 2023/03/20 17:26:33           
#                                                                              
# **************************************************************************** 

import xml.etree.ElementTree as ET
import argparse

# Appel programme en liste de commande

argParser = argparse.ArgumentParser()
# Création argument obligatoire : nom du fichier XML à traiter
argParser.add_argument('filename', help="XML file")
args = argParser.parse_args()

fichier_traite = args.filename

def extract_titre(fichier):
    tree = ET.parse(fichier)
    root = tree.getroot()

    nb_lignes = 1
    for element in root.iter('item'):
        for child in element:
            if child.tag=='title':
                print(nb_lignes, ':\n','Titre :',child.text)
            if child.tag=='description':
                print('Desc :',child.text)

        print('\n************************************************************************\n')
        nb_lignes+=1

extract_titre(fichier_traite)
