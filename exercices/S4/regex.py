import re
import pathlib
import argparse
argParser = argparse.ArgumentParser()
argParser.add_argument('filename', help="XML file")
args = argParser.parse_args()
fichier_traite = args.filename
def extraire(fichier):
    fic = pathlib.Path(fichier)
    xml = fic.read_text()
    L=[]
    pattern=re.findall(r"\<item\>.*?\<\/item>", xml)

    for article in pattern:
        titre=re.findall(r"(?P<first><title>)(?P<two>\<!\[CDATA\[)(?P<three>.*?)(?P<four>\]\]\>)(?P<five></title>)", article)
        descri=re.findall(r"(?P<first><description>)(?P<two>\<!\[CDATA\[)(?P<three>.*?)(?P<four>\]\]\>)(?P<five></description>)", article)
        print("titre = ",titre[0][2])
        print("description = ", descri[0][2])

extraire(fichier_traite)