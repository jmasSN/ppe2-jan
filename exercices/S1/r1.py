import argparse
import os

def extraire_lexique(chemin):
    # R1 (Aliénor Sauty de Chalon) :
    liste_n=[]
    def extraire(chemin):
        """fonction qui prend comme argument un chemin ( "exercices/S1/Corpus") et retourne une liste contenant le contenu de chaque fichier texte"""
        os.chdir(chemin)
        list_of_file=os.listdir()
        for file in list_of_file:
            mon_fichier = open(file,"r")
            contenu = mon_fichier.read()
            liste_n.append(contenu)
            mon_fichier.close()
        return liste_n

    contenu = extraire(chemin)
  
    print(contenu)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='permet le chemin')
    parser.add_argument('chemin', type=str, help='chemin du répertoire contenant les fichiers texte')
    args = parser.parse_args()
    chemin = args.chemin
    contenu = extraire_lexique(chemin)
    
    #python3 r1.py /Corpus/
