import argparse
import os
import sys

def extraire_lexique(chemin):
    # R1 (Aliénor Sauty de Chalon) :
    liste_n=[]
    def extraire(chemin):
        """fonction qui prend comme argument un chemin ( "exercices/S1/Corpus") et retourne une liste contenant le contenu de chaque fichier texte"""
        os.chdir(chemin)
        list_of_file=os.listdir()
        for file in list_of_file:
            mon_fichier = open(file,"r")
            contenu = mon_fichier.read()
            liste_n.append(contenu)
            mon_fichier.close()
        return liste_n
    contenu = extraire(chemin)
    print(contenu)


    # R2 Juliette MASSY
    #Pré traitement du corpus avec sed -e ':a' -e 'N' -e '$!ba' -i '' -e 's/\n/ /g' *.txt
    def lecture_corpus():
        """permet la lecture du corpus depuis l’entrée standard (un fichier par ligne)"""
        liste_n = []
        # 1 fichier/ligne
        for line in sys.stdin:
            liste_n.append(line)
        return liste_n

    # R3 Natacha MINICONI
    def extraire():
        """permet d'extraire le contenu des fichiers dans une liste , il se lance avec la commande ls Corpus/*.txt |python extraire_lexique.py"""
        liste_n = []
        for line in sys.stdin:
            line = line.strip()
            with open(line, "r") as mon_fichier:
                contenu = mon_fichier.read()
                liste_n.append(contenu)
        return liste_n

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='permet le chemin')
    parser.add_argument('chemin', type=str, help='chemin du répertoire contenant les fichiers texte')
    args = parser.parse_args()
    chemin = args.chemin
    contenu = extraire_lexique(chemin)
    
#python3 r1.py /Corpus/

if __name__ == "__main__":
    resultat = extraire()
    print(lecture_corpus)
    print(resultat)
