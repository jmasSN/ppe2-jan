# PPE2-JAN


## Constitution du groupe :

J.Massy

N.Miniconi

A. Sauty de Chalon

## Organisation :

La branche <i>main</i> constitue la branche principale de notre projet. Diverses branches ont été crées pendant la réalisation de ce projet et correspondent au travail individuel de chacune. Le <b>journal de bord</b> est disponible sur la branche <i>page</i>.
